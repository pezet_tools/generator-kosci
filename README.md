# Generator Kości
Aplikacja symulująca rzuty kością o dowolnej liczbie ścianek. 
Dodatkowo posiada opcję generowania wartości dla atrybutów postaci D&D. 

## Przygotowania
Do poprawnego działania aplikacji należy zainstalować javę w wersji 8 (jre1.8.0_202)

Do uruchomienia aplikacji np w IntelliJ należy zainstalować narzędzia deweloperskie javy w wersji 8 (jdk1.8.0_202):

| Opcje                                            | Opis                                                       |
|--------------------------------------------------|------------------------------------------------------------|
| File->Project Structure->Project->SDK            | Wybrać java sdk 8                                          |
| File->Project Structure->Project->Language level | Wybrać wersję 8                                            |
| File->Project Structure->Libraries               | Dodać pliki groovy do biblioteki (pliki jar, nie katalog)  |
| File->Project Structure->Libraries               | Dodać plik motywu jtattoo (plik jar)                       |


## Plik wykonywalny
Plik wykonywalny jar można uzyskać na dwa sposoby:
- Pobierając jego najnowszą wersją go [stąd](https://gitlab.com/pezet_tools/generator-kosci/-/tree/master/bin)
- Generując samemu na podstawie kodu źródłowego

