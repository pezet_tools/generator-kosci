import javax.swing.*
import java.awt.*
import groovy.swing.SwingBuilder


class Kosci{
	static b1 = 0
	static b2 = 0
	static kst
	static kst1
	static ilpowt
	static ilpowt1
	
	def static Kostki(){
	
		new SwingBuilder().edt {
			UIManager.setLookAndFeel("com.jtattoo.plaf.noire.NoireLookAndFeel");
		
			frame = frame(id: 'frame', title: 'Generator Kości',  size: [225, 325], defaultCloseOperation: JFrame.EXIT_ON_CLOSE,visible: true) {
				FlowLayout flowLayout = new FlowLayout(FlowLayout.LEADING)
				
				vbox(){
					label(' ')
						hbox(){
							label('Rodzaj kości:    K ')
							textArea(id: 'k', preferredSize: [25, 20], maximumSize: [25, 20], visible: true, editable: true)
							label('                          ')
						}
						label('')
						hbox(){
							label('Ilość powtórzeń:  ')
							textArea(id: 'i', preferredSize: [25, 20], maximumSize: [25, 20], visible: true, editable: true)
							label('                          ')
						}
					hbox(){
						button(text: 'Rzucaj', actionPerformed: {a->
							w1.setText('')
							try {
								kst = k.getText()
								kst1 = kst.toInteger()
								ilpowt = i.getText()
								ilpowt1 = ilpowt.toInteger()
								w1.append (Kosci.Rzuty())
							}
							catch (NumberFormatException exc) {
								w1.append ('Nie podano potrzebnej zmiennej\nliczbowej!')
							}
							catch (MissingMethodException exc) {
								w1.append ('Nie podano potrzebnej zmiennej\nliczbowej!')
							}
							catch (IllegalArgumentException exc) {
								w1.append ('Nie podano potrzebnej zmiennej\nliczbowej!')
							}
							frame.setVisible(true)
						})
						label('                                                ')
					}
					label(' ')
					textArea(id: 'w1', preferredSize: [200, 60], maximumSize: [200, 60], visible: true, editable:false)
					w1.setMargin(new Insets(5,5,5,5))
//					label(' ')
					label('Atrybuty D&D ')
					textArea(id: 'w2', preferredSize: [200, 20], maximumSize: [200, 20], visible: true, editable:false)
					w2.editable=false
					button(text: 'Rzucaj', actionPerformed: { a->
						w2.setText('')
						w2.append(Kosci.Atrybuty())
					})
					label(' ')
					label(' ')
					hbox(){
						label('  Autor ')
						label('<html><b><font color="white" size="4" face="Monotype Corsiva">pezet2</font></b></html>')
						label('           ')
						label('ver 1.0  ')
					}
				}
			}
		}
	}	
				
	def static Rzuty(){	
		
		def lista = []
		Random r = new Random()
		def suma = 0
		def srednia = 0
	
		for (int i = 0; i < ilpowt1; i++) {
			def kostka = r.nextInt(kst1)+1
			lista[i] = kostka
			suma += lista[i]
		}
		
		return "Rzuty kostką K" + kst1 + ":\n" + lista.join(', ') + "\n" + "SUMA = $suma"
	}

	def static Atrybuty () {
		def lista = []
		Random r = new Random()
	    def suma = 0
		def wynik = []
	    
		for (int j=0;j<7;j++){
			for (int i = 0; i < 4; i++) {
				def kostka = r.nextInt(6)+1
				lista[i] = kostka
				suma += lista[i]
			}
			
			def min = lista[0]
			for (int i = 1; i < lista.size(); i++) {
				if (lista[i] < min) min = lista[i]
			}
			suma = suma - min
			wynik << suma
			suma = 0
		}
		
		return wynik.join(', ')
	}
}

Kosci.Kostki()
